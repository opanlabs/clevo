function openNav() {
    document.getElementById('myNav').style.width = '100%';
}

function closeNav() {
    document.getElementById('myNav').style.width = '0%';
}


var currentX = '';
var currentY = '';
var movementConstant = .015;
$(document).mousemove(function(e) {
  if(currentX == '') currentX = e.pageX;
  var xdiff = e.pageX - currentX;
  currentX = e.pageX;
   if(currentY == '') currentY = e.pageY;
  var ydiff = e.pageY - currentY;
  currentY = e.pageY; 
  $('.parallax div').each(function(i, el) {
      var movement = (i + 1) * (xdiff * movementConstant);
    var movementy = (i + 1) * (ydiff * movementConstant);
      var newX = $(el).position().left + movement;
    var newY = $(el).position().top + movementy;
      $(el).css('left', newX + 'px');
    $(el).css('top', newY + 'px');
  });
});


$( document ).ready(function() {
  $('.single-item').slick({
    infinite: true,
    autoplay:true,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade:true,
    dots: true,
    prevArrow: '<i class="fa fa-chevron-left prev" aria-hidden="true"></i>',
    nextArrow: '<i class="fa fa-chevron-right next" aria-hidden="true"></i>',
  }); 

  $('.products-slide').slick({
    centerMode:true,
    slidesToShow: 3,
    dots: true,
    prevArrow: '<i class="fa fa-chevron-left prev" aria-hidden="true"></i>',
    nextArrow: '<i class="fa fa-chevron-right next" aria-hidden="true"></i>',
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          slidesToShow: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          slidesToShow: 1
        }
      }
    ]
  });

  $('.table-wrapper').jScrollPane();
});


$(window).scroll(function(){
	if($(document).scrollTop() > 0) {
	    $('#header').addClass('small');
	} else {
	    $('#header').removeClass('small');
	}
});

$(document).on('msf:viewChanged', function(event, data){
  var progress = Math.round((data.currentIndex / data.totalSteps)*100);
  $('.progress-bar').css('width', progress + '%').attr('aria-valuenow', progress);   ;
})

$('.msf:first').multiStepForm();


